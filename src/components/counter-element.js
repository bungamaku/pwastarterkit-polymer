/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html } from '@polymer/lit-element';

// These are the elements needed by this element.
import { plusIcon, minusIcon } from './my-icons.js';

// These are the shared styles needed by this element.
import { ButtonSharedStyles } from './button-shared-styles.js';

// This is a reusable element. It is not connected to the store. You can
// imagine that it could just as well be a third-party element that you
// got from someone else.
class CounterElement extends LitElement {
  _render(props) {
    return html`
      ${ButtonSharedStyles}
      <style>
        span { width: 20px; display: inline-block; text-align: center; font-weight: bold;}
      </style>
      <div>
        <p>
          Clicked: <span>${props.clicks}</span> times.
          Value is <span>${props.value}</span>.
          <button on-click="${() => props.onIncrement()}" title="Add 1">${plusIcon}</button>
          <button on-click="${() => props.onDecrement()}" title="Minus 1">${minusIcon}</button>
          <button on-click="${() => props.onMultiply(2)}" title="Double Value"><b>(x2)</b></button>
          <button on-click="${() => props.onMultiply(3)}" title="Triple Value"><b>(x3)</b></button>          
          <button on-click="${() => props.onMultiply(4)}" title="Quadruple Value"><b>(x4)</b></button>          
          <button on-click="${() => props.onMultiply(5)}" title="Quintuple Value"><b>(x5)</b></button>          
        </p>
      </div>
    `;
  }




  /*

  counter-element 
        -> event -> myview2 --dispatch--> store


        
  counter-element  <-callback- myview2
        |
      dispatch
        v
      store

  */

  static get properties() {
    return {
      /* The total number of clicks you've done. */
      clicks: Number,
      /* The current value of the counter. */
      value: Number,
      onIncrement: Function,
      onDecrement: Function,
      onMultiply: Function,
    }
  };
}

window.customElements.define('counter-element', CounterElement);
